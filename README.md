# ekrew project

## apps

ekrewniaczek  
ekrewniak  

## setup

`$ make install`  

## clean

`$ make clean`  

## run

`$ make run-${app_name}`  

## build

`$ make build-${app_name}`  

## test serving build

`$ make serve-dist-${app_name}`  

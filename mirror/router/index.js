import Vue from 'vue';
import VueRouter from 'vue-router';

import store from '@/store';

import routes from '@/router/routes.js';

import { TOGGLED_DRAWER } from '@/shared/types/actions';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  /* reset drawer state on page change */
  store.dispatch(TOGGLED_DRAWER, false);
  next();
});

export default router;

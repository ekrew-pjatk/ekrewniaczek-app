const BASE_URL = process.env.VUE_APP_BASE_URL;

const API_AUTHENTICATE_USER = `${BASE_URL}/centerusers/login`;
const API_ALERT_POST = `${BASE_URL}/alerts`;
const API_BLOOD_RESOURCES_PUT = (id) => `${BASE_URL}/bloodresources/${id}`;
const API_MESSAGES_GET = `${BASE_URL}/shoutbox`;
const API_MESSAGES_POST = `${BASE_URL}/shoutbox`;

export {
  API_AUTHENTICATE_USER,
  API_ALERT_POST,
  API_BLOOD_RESOURCES_PUT,
  API_MESSAGES_GET,
  API_MESSAGES_POST
};

import { POST_ALERT } from '@/types/actions';
import { API_ALERT_POST } from '@/types/endpoints';

const state = {};

const mutations = {};

const actions = {
  async [POST_ALERT] ({ rootState, commit }, payload) {
    const response = await fetch(API_ALERT_POST, {
      method: 'POST',
      headers: {
        'Origin': 'http://localhost:8001',
        'Content-Type': 'application/json',
        'Authorization': rootState.authModule.user.token
      },
      body: JSON.stringify(payload)
    });

    if ([200, 201].includes(response.status)) {
      return { err: { type: 'ok' } };
    } else if ([401, 403].includes(response.status)) {
      return { err: { type: 'unauthorized' } };
    } else if (response.status === 500) {
      return { err: { type: 'bad' } };
    } else {
      return { err: { type: 'other' } };
    }
  }
};

const getters = {};

export {
  state,
  mutations,
  actions,
  getters
};

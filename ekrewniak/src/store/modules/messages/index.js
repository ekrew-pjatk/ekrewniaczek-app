import { FETCH_MESSAGES, SEND_MESSAGE } from '@/types/actions';
import { API_MESSAGES_POST, API_MESSAGES_GET } from '@/types/endpoints';
import { GET_MESSAGES } from '@/types/getters';
import { SET_MESSAGES, SET_MESSAGE } from '@/types/mutations';

const state = {
  messages: []
};

const mutations = {
  [SET_MESSAGES] (state, messages) {
    state.messages = messages;
  },
  [SET_MESSAGE] (state, message) {
    state.messages.push(message);
  }
};

const actions = {
  async [FETCH_MESSAGES] ({ rootState, commit }, page) {
    page = page || 0;

    const response = await fetch(`${API_MESSAGES_GET}?limit=25&page=${page}`, {
      headers: {
        'Origin': 'http://localhost:8001',
        'Accept': 'application/json',
        'Authorization': rootState.authModule.user.token
      }
    });
    const messages = await response.json();

    commit(SET_MESSAGES, messages);
  },
  async [SEND_MESSAGE] ({ rootState, commit }, payload) {
    const response = await fetch(API_MESSAGES_POST, {
      method: 'POST',
      headers: {
        'Origin': 'http://localhost:8001',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': rootState.authModule.user.token
      },
      body: JSON.stringify(payload)
    });

    if ([200, 201].includes(response.status)) {
      const newMessage = await response.json();
      commit(SET_MESSAGE, newMessage);
    }
  }
};

const getters = {
  [GET_MESSAGES] (state) {
    return state.messages;
  }
};

export {
  state,
  mutations,
  actions,
  getters
};

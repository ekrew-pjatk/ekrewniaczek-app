import commonModules from './common';
import * as authModule from './auth';
import * as exclusiveAlertsModule from './alerts';
import * as exclusiveCenterModule from './center';
import * as exclusiveMessagesModule from './messages';

const selfModules = {
  authModule,
  exclusiveAlertsModule,
  exclusiveCenterModule,
  exclusiveMessagesModule
};

const modules = {
  ...commonModules,
  ...selfModules
};

export default modules;

import {
  SET_USER,
  UNSET_USER,
  SET_MY_CENTER,
  SET_MY_CENTER_BLOOD
} from '@/types/mutations';

import {
  FETCH_MY_CENTER,
  FETCH_MY_CENTER_BLOOD,
  LOGIN_USER,
  LOGOUT_USER
} from '@/types/actions';

import { GET_USER, IS_LOGGED_IN } from '@/types/getters';

import { API_AUTHENTICATE_USER } from '@/types/endpoints';

import { GUEST } from '@/const';

const NO_USER = {
  login: GUEST,
  token: ''
};

const INITIAL_STATE = () => {
  const user = localStorage.getItem('user');
  return (user != null) ? JSON.parse(user) : NO_USER;
};

const state = {
  user: INITIAL_STATE()
};

const mutations = {
  [SET_USER] (state, user) {
    state.user = user;
  },
  [UNSET_USER] (state) {
    state.user = NO_USER;
  }
};

const actions = {
  async [LOGIN_USER] ({ dispatch, commit }, payload) {
    const response = await fetch(API_AUTHENTICATE_USER, {
      method: 'POST',
      headers: {
        'Origin': 'http://localhost:8001',
        'Accept': '*/*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    });

    const status = response.status;
    if (status === 200) {
      const token = response.headers.get('authorization');
      const centeruserid = response.headers.get('centeruserid');

      if (token != null) {
        const user = {
          login: payload.login,
          token: token,
          centeruserid: centeruserid
        };

        commit(SET_USER, user);
        localStorage.setItem('user', JSON.stringify(user));

        /* dispatch actions from other module from root */
        await dispatch(FETCH_MY_CENTER, null, { root: true });
        await dispatch(FETCH_MY_CENTER_BLOOD, null, { root: true });

        return user;
      } else {
        return { err: { type: 'token' } };
      }
    } else if (status === 403) {
      return { err: { type: 'credentials' } };
    } else {
      return { err: { type: 'other' } };
    }
  },
  async [LOGOUT_USER] ({ commit, router }) {
    commit(UNSET_USER);
    commit(SET_MY_CENTER_BLOOD, {});
    commit(SET_MY_CENTER, {});

    localStorage.removeItem('user');
  }
};

const getters = {
  [GET_USER]: state => {
    return state.user;
  },
  [IS_LOGGED_IN]: state => (!!state.user.token)
};

export {
  state,
  mutations,
  actions,
  getters
};

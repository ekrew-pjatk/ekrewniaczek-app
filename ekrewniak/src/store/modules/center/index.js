import { SET_MY_CENTER, SET_MY_CENTER_BLOOD } from '@/types/mutations';
import { GET_MY_CENTER, GET_MY_CENTER_BLOOD } from '@/types/getters';

import {
  FETCH_MY_CENTER,
  FETCH_MY_CENTER_BLOOD,
  UPDATE_MY_CENTER_BLOOD
} from '@/types/actions';

import { API_BLOOD_RESOURCES_PUT } from '@/types/endpoints';
import { API_BLOOD_RESOURCES_GET } from '@/shared/types/endpoints';

import { unwrap } from '@/shared/utils/api';

const state = {
  center: {},
  bloodResources: []
};

const mutations = {
  [SET_MY_CENTER] (state, center) {
    state.center = center;
  },
  [SET_MY_CENTER_BLOOD] (state, blood) {
    state.bloodResources = blood;
  }
};

const actions = {
  [FETCH_MY_CENTER] ({ rootState, commit }) {
    const email = rootState.authModule.user.login;
    const centers = rootState.centersModule.centers;
    const myCenter = unwrap(centers.filter(c => c.email === email));

    commit(SET_MY_CENTER, myCenter);
    return myCenter;
  },
  async [FETCH_MY_CENTER_BLOOD] ({ state, commit }) {
    const { centerId } = state.center;
    const url = API_BLOOD_RESOURCES_GET(centerId);
    const response = await fetch(url, {
      headers: {
        'Accept': 'application/json'
      }
    });

    const bloodResources = unwrap(await response.json());
    if (Array.isArray(bloodResources)) {
      const mappedBloodResources = bloodResources.map(br => ({
        resourceId: br.bloodResourceId,
        blood: br.blood,
        volume: br.volume
      }));
      commit(SET_MY_CENTER_BLOOD, mappedBloodResources);

      return mappedBloodResources;
    }
  },
  async [UPDATE_MY_CENTER_BLOOD] ({ rootState, commit }, payload) {
    const { bloodResources, resourceId, volume } = payload;

    commit(SET_MY_CENTER_BLOOD, bloodResources);
    const url = API_BLOOD_RESOURCES_PUT(resourceId);
    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        'Origin': 'http://localhost:8001',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': rootState.authModule.user.token
      },
      body: JSON.stringify({ volume: volume })
    });

    return response;
  }
};

const getters = {
  [GET_MY_CENTER]: state => {
    return state.center;
  },
  [GET_MY_CENTER_BLOOD]: state => {
    return state.bloodResources;
  }
};

export {
  state,
  mutations,
  actions,
  getters
};

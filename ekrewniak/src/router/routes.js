import Info from '@/components/Info.vue';
import AlertForm from '@/components/AlertForm.vue';
import Centers from '@/shared/components/Centers.vue';
import Partners from '@/components/Partners.vue';
import Login from '@/components/Login.vue';
import MyBloodResources from '@/components/MyBloodResources';
import Messages from '@/components/Messages';

import store from '@/store';

import { IS_LOGGED_IN } from '@/types/getters';

const isLoggedIn = (to, from, next) => {
  if (store.getters[IS_LOGGED_IN]) {
    next();
  } else {
    next('login');
  }
};

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
    icon: 'mdi-login',
    hidden: true
  },
  {
    path: '/info',
    name: 'info',
    component: Info,
    icon: 'mdi-information-outline',
    beforeEnter: isLoggedIn,
    disabled: true
  },
  {
    path: '/alert-post',
    name: 'alert-post',
    component: AlertForm,
    icon: 'mdi-bell-outline',
    beforeEnter: isLoggedIn,
    disabled: true
  },
  {
    path: '/blood-resources',
    name: 'blood-resources',
    component: MyBloodResources,
    icon: 'mdi-flask-outline',
    beforeEnter: isLoggedIn,
    disabled: true
  },
  {
    path: '/messages',
    name: 'messages',
    component: Messages,
    icon: 'mdi-message-text-outline',
    beforeEnter: isLoggedIn,
    disabled: true
  },
  {
    path: '/centers',
    name: 'centers',
    component: Centers,
    icon: 'mdi-hospital-box-outline'
  },
  {
    path: '/partners',
    name: 'partners',
    component: Partners,
    icon: 'mdi-briefcase-outline'
  },
  {
    path: '*',
    redirect: 'login'
  }
];

export default routes;

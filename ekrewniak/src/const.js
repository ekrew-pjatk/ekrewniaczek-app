const GUEST = 'guest';
const LOG_IN = 'log in';
const LOG_OUT = 'log out';

export { GUEST, LOG_IN, LOG_OUT };

module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  setupFiles: ['./tests/config.js']
};

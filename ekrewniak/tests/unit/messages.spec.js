import Vuetify from "vuetify";
import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Messages from '@/components/Messages';

import { GET_MESSAGES } from '@/types/getters';
import { FETCH_MESSAGES, SEND_MESSAGE } from '@/types/actions';
import messages from '../fixtures/messages.json';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('Messages', () => {
  let getters;
  let store;
  let actions;
  let wrapper;
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
    getters = {
      [GET_MESSAGES]: () => messages,
    }
    actions = {
      [FETCH_MESSAGES]: jest.fn(),
      [SEND_MESSAGE]: jest.fn(),
    }

    store = new Vuex.Store({
      actions,
      getters
    });

    wrapper = shallowMount(Messages, {
      mocks: {
        $i18n: { locale: 'pl' },
        $t: () => 'pl'
      },
      store,
      localVue,
      vuetify
    });
  })

  it('should render existing messages', () => {
    const messages = wrapper.findAll('#table-body tr');
    expect(messages.at(0).text()).toBe('23/05/2020 12:54:46  Wiadomość do innych centrów krwiodawstwa.');
    expect(messages.at(1).text()).toBe('23/05/2020 12:55:00  Kolejna wiadomość do centrów.');
  });
});

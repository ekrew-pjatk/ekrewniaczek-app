import Vuetify from "vuetify";
import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Info from '@/components/Info';

import { GET_MY_CENTER, GET_MY_CENTER_BLOOD } from '@/types/getters';
import center from '../fixtures/center.json';
import centerBlood from '../fixtures/centerBlood.json';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('Alert Details', () => {
  let getters;
  let store;
  let wrapper;
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
    getters = {
      [GET_MY_CENTER]: () => center,
      [GET_MY_CENTER_BLOOD]: () => centerBlood,
    }

    store = new Vuex.Store({
      getters
    });

    wrapper = shallowMount(Info, {
      mocks: {
        $t: key => key
      },
      stubs: ['router-link'],
      store,
      localVue,
      vuetify
    });
  })

  it('should render center details', () => {
    const centerName = wrapper.find('.caption');
    expect(centerName.text()).toContain('krew@gda.pl');
    expect(centerName.text()).toContain('+48(58)5204010');
    expect(centerName.text()).toContain('https://krew.gda.pl');
  });

  it('should render correct number of center blood', () => {
    const centerBlood = wrapper.findAll('.blood-block');
    expect(centerBlood).toHaveLength(8);
  });

  it('should render correct groups', () => {
    const centerBlood = wrapper.findAll('.blood-block .red--text');
    expect(centerBlood.at(0).text()).toBe('0-');
    expect(centerBlood.at(7).text()).toBe('AB+');
  });

  it('should render correct groups values', () => {
    const centerBlood = wrapper.findAll('.blood-block span:nth-child(2)');
    expect(centerBlood.at(0).text()).toBe('500');
    expect(centerBlood.at(7).text()).toBe('300');
  });
});

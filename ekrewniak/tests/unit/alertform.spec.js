import Vuetify from "vuetify";
import Vuex from 'vuex';
import { mount, createLocalVue } from '@vue/test-utils';
import AlertForm from '@/components/AlertForm';

import { GET_BLOOD_GROUPS } from '@/shared/types/getters';
import { FETCH_BLOOD_GROUPS} from '@/shared/types/actions';
import { POST_ALERT } from '@/types/actions';
import bloodGroups from '../fixtures/bloodGroups.json';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('Alert Details', () => {
  let actions;
  let getters;
  let store;
  let wrapper;
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
    getters = {
      [GET_BLOOD_GROUPS]: () => bloodGroups,
    }
    actions = {
      [FETCH_BLOOD_GROUPS]: jest.fn(),
      [POST_ALERT]: () => new Promise(res => res({
        response: 'test'
      }))
    }

    store = new Vuex.Store({
      getters,
      actions
    });

    wrapper = mount(AlertForm, {
      mocks: {
        $t: () => {}
      },
      store,
      localVue,
      vuetify
    });
  })

  it('should initialize blood groups', () => {
    expect(wrapper.find({ name: 'v-select' }).props('items')).toStrictEqual([
      "0-", "0+", "A-", "A+", "B-", "B+", "AB-", "AB+"
    ]);
  });

  it('should display error for empty message', async () => {
    wrapper.setData({
      selectedBloodType: "0-"
    });

    await wrapper.find('.v-btn').trigger('click');
    expect(wrapper.find('.alert-error').exists()).toBe(true);
  });

  it('should not display error if message was set', async () => {
    wrapper.setData({
      selectedBloodType: "0-",
      message: "Potrzeba krwi"
    });

    await wrapper.find('.v-btn').trigger('click');
    expect(wrapper.find('.alert-error').exists()).toBe(false);
  });
});

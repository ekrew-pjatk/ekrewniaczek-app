import commonModules from './common';
import * as preferencesModule from './preferences';
import * as alertsModule from './alerts';

// app exclusive modules
const selfModules = {
  preferencesModule,
  alertsModule
};

const modules = {
  ...commonModules,
  ...selfModules
};

export default modules;

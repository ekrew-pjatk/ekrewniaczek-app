import { SET_ALERTS, SET_ALERT_DETAIL, SET_PROXIMAL_ALERTS } from '@/shared/types/mutations';
import { GET_ALERT_DETAIL, GET_ALERTS, GET_PROXIMAL_ALERTS } from '@/shared/types/getters';
import { FETCH_ALERT, FETCH_ALERTS, FETCH_ALERTS_PROXIMAL, FETCH_ALERTS_PREFERRED } from '@/shared/types/actions';
import { API_ALERTS_GET, API_CENTERS_GET } from '@/shared/types/endpoints';
import { unwrap } from '@/shared/utils/api';

const state = {
  alerts: [],
  alertsProximal: [],
  alertDetail: {}
};

const mutations = {
  [SET_ALERTS] (state, alerts) {
    state.alerts = alerts;
  },
  [SET_PROXIMAL_ALERTS] (state, alerts) {
    state.alertsProximal = alerts;
  },
  [SET_ALERT_DETAIL] (state, alert) {
    state.alertDetail = alert;
  }
};

const actions = {
  [FETCH_ALERTS_PROXIMAL] ({ dispatch, commit, getters }, payload) {
    dispatch(FETCH_ALERTS, payload)
      .then(alerts => commit(SET_PROXIMAL_ALERTS, alerts));
  },
  [FETCH_ALERTS_PREFERRED] ({ dispatch, commit }, payload) {
    dispatch(FETCH_ALERTS, payload)
      .then(alerts => commit(SET_ALERTS, alerts));
  },

  async [FETCH_ALERTS] ({ rootState }, payload) {
    const { query, page = 0 } = payload;

    const querySign = query ? `&` : `?`;
    const pageQueryString = `${querySign}page=${page}`;

    const response = await fetch(API_ALERTS_GET + query + pageQueryString, {
      headers: {
        'Accept': 'application/json'
      }
    });
    const alerts = await response.json();

    if (!alerts) {
      return;
    }

    let centersCache = rootState.centersModule.centersMap;
    let alertsWithCenters = await Promise.all(alerts.map(alert => {
      const { centerId } = alert;
      if (Object.keys(centersCache).includes(centerId)) {
        return { ...alert, center: centersCache[centerId] };
      }

      return fetch(`${API_CENTERS_GET}/${centerId}`, {
        headers: {
          'Accept': 'application/json'
        }
      })
        .then(response => response.json())
        .then(center => ({ ...alert, center: center }));
    }));

    return alertsWithCenters;
  },
  async [FETCH_ALERT] ({ state, commit }, id) {
    const alertPreferred = state.alerts.find(alert => alert.alertId === id);
    const alertProximal = state.alertsProximal.find(alert => alert.alertId === id);

    if (alertPreferred || alertProximal) {
      commit(SET_ALERT_DETAIL, unwrap(alertPreferred || alertProximal));
    } else {
      const alertResponse = await fetch(`${API_ALERTS_GET}/${id}`, {
        headers: {
          'Accept': 'application/json'
        }
      });
      const alert = await alertResponse.json();

      const centerResponse = await fetch(`${API_CENTERS_GET}/${alert.centerId}`, {
        headers: {
          'Accept': 'application/json'
        }
      });
      const center = await centerResponse.json();
      commit(SET_ALERT_DETAIL, unwrap({ ...alert, center: center }));
    }
  }
};

const getters = {
  [GET_ALERTS]: state => state.alerts,
  [GET_PROXIMAL_ALERTS]: state => state.alertsProximal,
  [GET_ALERT_DETAIL]: state => state.alertDetail
};

export {
  state,
  mutations,
  actions,
  getters
};

import { GET_PREFERENCES } from '@/shared/types/getters';
import { SET_PREFERENCES } from '@/shared/types/mutations';

import {
  GET_SELECTED_BLOOD_TYPES,
  GET_SELECTED_CENTERS,
  GET_GPS_ALLOWED,
  GET_RADIUS
} from '@/types/getters';

const NO_PREFERENCES = {};

const INITIAL_STATE = () => {
  const preferences = localStorage.getItem('preferences');
  return (preferences != null) ? JSON.parse(preferences) : NO_PREFERENCES;
};

const state = {
  preferences: INITIAL_STATE()
};

const mutations = {
  [SET_PREFERENCES] (state, preferences) {
    state.preferences = preferences;
    localStorage.setItem('preferences', JSON.stringify(preferences));
  }
};

const actions = {};

const getters = {
  [GET_PREFERENCES]: state => state.preferences,
  [GET_SELECTED_BLOOD_TYPES]: state => state.preferences.selectedBloodTypes
    .flat()
    .filter(blood => blood.selected)
    .map(blood => blood.type),
  [GET_SELECTED_CENTERS]: state => state.preferences.selectedCenters,
  [GET_GPS_ALLOWED]: state => state.preferences.enableGps,
  [GET_RADIUS]: state => state.preferences.alertsRange
};

export {
  state,
  mutations,
  actions,
  getters
};

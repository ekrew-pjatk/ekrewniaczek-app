export const alertTypesEnum = {
  PREFERRED_ALERTS: 'preferredAlerts',
  PROXIMAL_ALERTS: 'proximalAlerts'
};

import Info from '@/components/Info.vue';
import Alerts from '@/components/Alerts.vue';
import Centers from '@/shared/components/Centers.vue';
import Partners from '@/components/Partners.vue';
import Preferences from '@/components/Preferences.vue';
import Neighbourhood from '@/components/Neighbourhood.vue';
import AlertDetail from '@/components/AlertDetail';

const routes = [
  {
    path: '/requirements',
    name: 'requirements',
    component: Info,
    icon: 'mdi-information-outline'
  },
  {
    path: '/alerts',
    name: 'alerts',
    component: Alerts,
    icon: 'mdi-bell-outline',
    children: [
      {
        path: ':id',
        component: AlertDetail,
        name: 'alertDetail'
      }
    ]
  },
  {
    path: '/centers',
    name: 'centers',
    component: Centers,
    icon: 'mdi-hospital-box-outline'
  },
  {
    path: '/map',
    name: 'neighbourhood',
    component: Neighbourhood,
    icon: 'mdi-hospital-marker'
  },
  {
    path: '/preferences',
    name: 'preferences',
    component: Preferences,
    icon: 'mdi-cog-outline'
  },
  {
    path: '/partners',
    name: 'partners',
    component: Partners,
    icon: 'mdi-briefcase-outline'
  },
  {
    path: '*',
    redirect: 'alerts'
  }
];

export default routes;

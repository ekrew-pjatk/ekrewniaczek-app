// app exclusive types
import * as getters from './getters';

const actions = {};
const endpoints = {};
const mutations = {};

export {
  actions,
  getters,
  endpoints,
  mutations
};

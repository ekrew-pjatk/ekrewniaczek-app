import dictionary from '@/locales/pl.json';
import { get } from 'lodash';
import { config } from '@vue/test-utils'

import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)

config.mocks["$t"] = (msg) => get(dictionary, msg);

const mockGeolocation = {
  getCurrentPosition: jest.fn(),
  watchPosition: jest.fn()
};

global.navigator.geolocation = mockGeolocation;

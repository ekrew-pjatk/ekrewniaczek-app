import Vuetify from 'vuetify';
import Center from '@/shared/components/Center';
import center from '../fixtures/center.json';
import { shallowMount, createLocalVue } from '@vue/test-utils';

const localVue = createLocalVue();

describe('Center tests', () => {
  let vuetify;
  let wrapper;

  beforeEach(()=> {
    vuetify = new Vuetify();
    wrapper = shallowMount(Center, {
      mocks: {
        $i18n: { locale: 'pl' },
        $t: (key) => key
      },
      localVue,
      vuetify,
      propsData: {
        center: center
      }
    });
  })

  it('should render center shortname', () => {
    const centerHeader = wrapper.findComponent({ name: 'v-expansion-panel-header'});
    expect(centerHeader.text()).toBe('RCKiK w Gdańsku');
  });

  it('should render center name', () => {
    const centerSubtitleName = wrapper.find('.subtitle-1');
    expect(centerSubtitleName.text()).toBe('Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Gdańsku');
  });

  it('should render center address', () => {
    const centerSubtitleAddress = wrapper.find('.subtitle-2');
    expect(centerSubtitleAddress.text()).toBe('Józefa Hoene-Wrońskiego 4, 80-210 Gdańsk');
  });

  it('should render center details', () => {
    const centerDetails = wrapper.find('.caption');
    expect(centerDetails.text()).toContain('sekretariat@rckik.gdansk.pl');
    expect(centerDetails.text()).toContain('+48(58)5204010');
    expect(centerDetails.text()).toContain('https://krew.gda.pl');
  });
});

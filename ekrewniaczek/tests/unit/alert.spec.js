import Vuetify from 'vuetify';
import Alert from '@/components/Alert';
import alert from '../fixtures/alert.json'
import { shallowMount, createLocalVue } from '@vue/test-utils';

const localVue = createLocalVue();

describe('Alert', () => {
  beforeAll(()=> {
    const mockCurrentDate = new Date('2020-05-23T04:41:20');
    jest.spyOn(Date, 'now')
    .mockImplementation(() => mockCurrentDate);
  });

  let vuetify;
  let wrapper;

  beforeEach(()=> {
    vuetify = new Vuetify();
    wrapper = shallowMount(Alert, {
      localVue,
      vuetify,
      propsData: {
        alert: alert
      }
    });
  })

  it('sets router link', () => {
    const alert = wrapper.findComponent({ name: 'v-list-item'});
    expect(alert.attributes().to).toBe('/alerts/123');
  });

  it('sets center name in title', () => {
    const centerName = wrapper.find('.alert-title');
    expect(centerName.text()).toBe('RCKiK, Gdańsk');
  });

  it('sets blood type in subtitle', () => {
    const centerName = wrapper.find('.subtitle-2');
    expect(centerName.text()).toBe('A+');
  });

  it('sets time ago value', () => {
    const centerName = wrapper.find('.time-ago');
    expect(centerName.text()).toBe('2 dni temu');
  });
});

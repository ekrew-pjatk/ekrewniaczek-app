import Vuetify from "vuetify";
import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Preferences from '@/components/Preferences';

import { GET_BLOOD_GROUPS, GET_CENTERS, GET_PREFERENCES } from '@/shared/types/getters';
import { FETCH_BLOOD_GROUPS, FETCH_CENTERS } from '@/shared/types/actions';

import centers from '../fixtures/centers.json';
import bloodGroups from '../fixtures/bloodGroups.json';


const localVue = createLocalVue();
localVue.use(Vuex);

describe('Alert Details', () => {
  let actions;
  let getters;
  let store;
  let wrapper;
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
    getters = {
      [GET_BLOOD_GROUPS]: () => bloodGroups,
      [GET_CENTERS]: () => centers,
      [GET_PREFERENCES]: function () {
        return {
          alertsRange: 50
        }
      }
    }
    actions = {
      [FETCH_BLOOD_GROUPS]: jest.fn(),
      [FETCH_CENTERS]: jest.fn()
    }

    store = new Vuex.Store({
      getters,
      actions
    });

    wrapper = shallowMount(Preferences, {
      store,
      localVue,
      vuetify
    });
  })

  it('should initialize centers', () => {
    expect(wrapper.findComponent({ name: 'v-combobox' }).props('items')).toHaveLength(2);
    expect(wrapper.findComponent({ name: 'v-combobox' }).props('items')[0]).toBe('RCKiK w Białymstoku');
  });
});

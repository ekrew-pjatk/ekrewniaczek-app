import { shallowMount, createLocalVue } from '@vue/test-utils';
import { GET_ALERT_DETAIL } from '@/shared/types/getters';
import { FETCH_ALERT } from '@/shared/types/actions';

import AlertDetail from '@/components/AlertDetail';
import alert from '../fixtures/alert.json';

import Vuex from 'vuex';
import vuetify from "vuetify";

const localVue = createLocalVue();
localVue.use(vuetify);
localVue.use(Vuex);

describe('Alert Details', () => {
  let actions;
  let getters;
  let store;
  let wrapper;

  const $route = {
    params: {
      id: 123
    }
  }

  beforeEach(() => {
    getters = {
      [GET_ALERT_DETAIL]: () => alert
    }
    actions = {
      [FETCH_ALERT]: jest.fn()
    }

    store = new Vuex.Store({
      getters,
      actions
    });

    wrapper = shallowMount(AlertDetail, {
      mocks: {
        $route
      },
      store,
      localVue
    });
  })

  it('sets alert title', () => {
    const alertTitle = wrapper.find('.title');
    expect(alertTitle.text()).toBe('Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Gdańsku');
  });

  it('sets alert subtitle', () => {
    const alertSubtitle = wrapper.find('.subtitle-1');
    expect(alertSubtitle.text()).toBe('4 Maja 2020,12:32:58');
  });

  it('sets message', () => {
    const details = wrapper.find('.text--primary');
    expect(details.text()).toBe('potrzeba krwi');
  });

  it('sets blood type', () => {
    const blood = wrapper.find('.subtitle-2');
    expect(blood.text()).toBe('A+');
  });
});

import { shallowMount, createLocalVue } from '@vue/test-utils';
import CenterPopup from '@/components/CenterPopup';
import center from '../fixtures/center.json'
import vuetify from "vuetify";

const localVue = createLocalVue()
localVue.use(vuetify)

describe('CenterPopup', () => {
  let wrapper;
  beforeEach(()=>{
    wrapper = shallowMount(CenterPopup, {
      localVue,
      propsData: {
        center: center
      }
    });
  })

  it('sets center name in popoup', () => {
    const centerName = wrapper.find('.subtitle-2');
    expect(centerName.text()).toBe('Regionalne Centrum Krwiodawstwa i Krwiolecznictwa w Gdańsku');
  });

  it('sets center name in title', () => {
    const centerStreet = wrapper.find('.center-street');
    expect(centerStreet.text()).toBe('Józefa Hoene-Wrońskiego 4');
  });

  it('sets address in content', () => {
    const centerAddress = wrapper.find('.center-address');
    expect(centerAddress.text()).toBe('Gdańsk, 80-210');
  });

  it('sets website with href', () => {
    const centerWebsite = wrapper.find('.center-website');

    expect(centerWebsite.text()).toBe('www: https://krew.gda.pl');
    expect(centerWebsite.find('a').attributes().href).toBe('https://krew.gda.pl');
  });

  it('sets phone with tel: href', () => {
    const centerWebsite = wrapper.find('.center-phone');

    expect(centerWebsite.text()).toBe('Tel: +48(58)5204010');
    expect(centerWebsite.find('a').attributes().href).toBe('tel:+48(58)5204010');
  });
});

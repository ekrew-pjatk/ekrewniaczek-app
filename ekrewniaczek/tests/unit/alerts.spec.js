import { shallowMount, createLocalVue } from '@vue/test-utils'
import Alerts from '@/components/Alerts';
import Alert from '@/components/Alert';
import proximalAlerts from '../fixtures/proximalAlerts';
import preferredAlerts from '../fixtures/preferredAlerts';
import { GET_ALERTS, GET_PROXIMAL_ALERTS, GET_PREFERENCES } from '@/shared/types/getters';
import { GET_SELECTED_CENTERS, GET_SELECTED_BLOOD_TYPES, GET_RADIUS } from '@/types/getters';
import { FETCH_ALERTS_PREFERRED, FETCH_ALERTS_PROXIMAL } from '@/shared/types/actions';

import vuetify from "vuetify";
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(vuetify);
localVue.use(Vuex);

describe('Alerts', () => {
  let actions;
  let getters;
  let store;
  let wrapper;

  beforeEach(() => {
    getters = {
      [GET_ALERTS]: () => preferredAlerts,
      [GET_PROXIMAL_ALERTS]: () => proximalAlerts,
      [GET_SELECTED_CENTERS]: () => [],
      [GET_SELECTED_BLOOD_TYPES]: () => [],
      [GET_RADIUS]: () => 100,
      [GET_PREFERENCES]: function () {
        return {
          enableGPS: true
        }
      }
    }
    actions = {
      [FETCH_ALERTS_PREFERRED]: jest.fn(),
      [FETCH_ALERTS_PROXIMAL]: jest.fn()
    }

    store = new Vuex.Store({
      getters,
      actions
    });

    wrapper = shallowMount(Alerts, {
      mocks: {
        $t: (key) => (key)
      },
      stubs: ['router-view'],
      store,
      localVue
    });
  })

  it('sets preferred alerts', () => {
    const preferredAlerts = wrapper.find('.preferred-alerts');
    expect(preferredAlerts.findAllComponents(Alert)).toHaveLength(3);
  });

  it('sets proximal alerts', () => {
    const preferredAlerts = wrapper.find('.proximal-alerts');
    expect(preferredAlerts.findAllComponents(Alert)).toHaveLength(2);
  });
});

import Vuetify from 'vuetify';
import BloodTable from '@/shared/components/BloodTable';
import bloodResources from '../fixtures/bloodResources.json';
import { shallowMount, createLocalVue } from '@vue/test-utils';

const localVue = createLocalVue();

describe('Blood Table tests', () => {
  let vuetify;
  let wrapper;

  beforeEach(()=> {
    vuetify = new Vuetify();
    wrapper = shallowMount(BloodTable, {
      mocks: {
        $i18n: { locale: 'pl' },
        $t: (key) => key
      },
      localVue,
      vuetify,
      propsData: {
        bloodResources: bloodResources
      }
    });
  })

  it('should render all table rows', () => {
    const tableRows = wrapper.findAll('tbody tr');
    expect(tableRows).toHaveLength(8);
  });

  it('should set severity to shortage', () => {
    const tableRows = wrapper.findAll('tbody tr td:last-child');
    expect(tableRows.at(0).text()).toBe('levels.shortage');
  });

  it('should set severity to critical', () => {
    const tableRows = wrapper.findAll('tbody tr td:last-child');
    expect(tableRows.at(2).text()).toBe('levels.critical');
  });

  it('should set severity to plenty', () => {
    const tableRows = wrapper.findAll('tbody tr td:last-child');
    expect(tableRows.at(3).text()).toBe('levels.plenty');
  });

  it('should set severity to medium', () => {
    const tableRows = wrapper.findAll('tbody tr td:last-child');
    expect(tableRows.at(1).text()).toBe('levels.medium');
  });
});

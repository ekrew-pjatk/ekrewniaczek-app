module.exports = {
  configureWebpack: {
    devtool: 'source-map'
  },

  chainWebpack: (config) => {
    config.resolve.symlinks(false)
  },

  'transpileDependencies': [
    'vuetify'
  ],

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  }
};

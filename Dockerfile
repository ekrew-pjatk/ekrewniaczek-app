FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y make
RUN apt-get install -y supervisor
RUN apt-get -y install curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_12.x  | bash -
RUN apt-get -y install nodejs

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN mkdir /var/apps
VOLUME /var/apps
WORKDIR /var/apps

CMD ["/usr/bin/supervisord"]

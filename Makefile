HTTP_SERVER_CMD = /usr/bin/env python -m SimpleHTTPServer

EKREWNIAK_DIST = ./ekrewniak/dist
EKREWNIACZEK_DIST = ./ekrewniaczek/dist


all:
	@:


### ekrewniaczek

install-ekrewniaczek:
	@(cd ekrewniaczek; npm install)

clean-ekrewniaczek:
	@(cd ekrewniaczek; rm -rf ./node_modules)

run-docker-ekrewniaczek:
	@(cd ekrewniaczek; npm install; npm run serve)

run-ekrewniaczek: ekrewniaczek/node_modules
	@(cd ekrewniaczek; npm run serve)

ekrewniaczek/dist/: ekrewniaczek/node_modules
	@(cd ekrewniaczek; npm run build)

serve-dist-ekrewniaczek: ekrewniaczek/dist/
	@(cd $(EKREWNIACZEK_DIST); $(HTTP_SERVER_CMD) 8111)

### ekrewniak

install-ekrewniak:
	@(cd ekrewniak; npm install)

clean-ekrewniak:
	@(cd ekrewniak; rm -rf ./node_modules)

run-docker-ekrewniak:
	@(cd ekrewniak; npm install; npm run serve)

run-ekrewniak: ekrewniak/node_modules
	@(cd ekrewniak; npm run serve)

ekrewniak/dist/: ekrewniak/node_modules
	@(cd ekrewniak; npm run build)

serve-dist-ekrewniak: ekrewniak/dist/
	@(cd $(EKREWNIAK_DIST); $(HTTP_SERVER_CMD) 8101)

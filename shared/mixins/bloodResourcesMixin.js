import { bloodResources } from '@/shared/enums/bloodResources';

export const bloodResourcesMixin = {
  data: function () {
    return {
      levels: {
        critical: bloodResources.CRITICAL,
        shortage: bloodResources.LOW,
        medium: bloodResources.MEDIUM,
        plenty: bloodResources.PLENTY
      }
    };
  },
  computed: {
    fullLevel () {
      return this.levels.plenty;
    }
  },
  methods: {
    getSeverity (volume) {
      for (const levelType in this.levels) {
        if (volume < this.levels[levelType]) {
          return levelType;
        }
      }

      return 'plenty';
    },
    getBloodType (resource) {
      const { bloodType, rh } = resource.blood;
      return bloodType + rh;
    }
  }
};

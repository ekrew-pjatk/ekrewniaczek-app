async function getCurrentLocation () {
  return new Promise(function (resolve, reject) {
    navigator.geolocation.getCurrentPosition(resolve, reject);
  });
}

function calculateDistance (locationStart, locationEnd) {
  const EARTH_RADIUS = 6371;
  const latStart = locationStart.lat;
  const lonStart = locationStart.lon;
  const latEnd = locationEnd.lat;
  const lonEnd = locationEnd.lon;

  const dLat = deg2rad(latEnd - latStart);
  const dLon = deg2rad(lonEnd - lonStart);

  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(latStart)) * Math.cos(deg2rad(latEnd)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);

  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  return EARTH_RADIUS * c;
}

function deg2rad (deg) {
  return deg * (Math.PI / 180);
}

function locationToNumber (location) {
  const lat = Number(location.latitude);
  const lon = Number(location.longitude);

  return { lat, lon };
}

export { getCurrentLocation, calculateDistance, locationToNumber };

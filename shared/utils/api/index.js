function unwrap (object) {
  if (Array.isArray(object)) {
    switch (object.length) {
      case 1: return object[0];
      case 0: return {};
    }
  } else {
    return object; // object
  }

  return object; // array of multiple elements
}

export { unwrap };

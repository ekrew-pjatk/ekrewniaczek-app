function calculateDelta (date) {
  let ms = Date.now() - new Date(date);

  const days = Math.floor(ms / 1000 / 60 / (60 * 24));
  ms -= days * 1000 * 60 * 60 * 24;
  const hours = Math.floor(ms / 1000 / 60 / 60);
  ms -= hours * 1000 * 60 * 60;
  const minutes = Math.floor(ms / 1000 / 60);
  ms -= minutes * 1000 * 60;
  const seconds = Math.floor(ms / 1000);
  ms -= seconds * 1000;

  return {
    days: days,
    hours: hours,
    minutes: minutes,
    seconds: seconds
  };
}

function dateParts (dateLocalized) {
  const date = new Date(dateLocalized);

  return {
    day: date.getDay() + 1,
    year: date.getFullYear(),
    month: date.toLocaleDateString('en-GB', { month: 'long' }),
    time: date.toTimeString().split(' ')[0]
  };
}

export {
  calculateDelta,
  dateParts
};

function makeAbbrev (name) {
  const cityDelimiterIdx = name.search(/\swe?/);
  const suffix = ' ' + (cityDelimiterIdx > 0) ? name.slice(cityDelimiterIdx) : '';

  if (suffix) {
    const nameAsArr = name.split(suffix)[0].split(' ');
    return nameAsArr.reduce((acc, s) => acc + s[0], '') + suffix;
  } else {
    return name.split(' ').reduce((acc, s) => acc + s[0], '');
  }
}

export { makeAbbrev };

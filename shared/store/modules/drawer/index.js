import { GET_DRAWER } from '@/shared/types/getters';
import { SET_DRAWER } from '@/shared/types/mutations';
import { TOGGLED_DRAWER } from '@/shared/types/actions';

const state = {
  drawerActive: false
};

const mutations = {
  [SET_DRAWER] (state, drawerState) {
    state.drawerActive = drawerState;
  }
};

const actions = {
  [TOGGLED_DRAWER] ({ commit }, drawerState) {
    commit(SET_DRAWER, drawerState);
  }
};

const getters = {
  [GET_DRAWER]: state => state.drawerActive
};

export {
  state,
  mutations,
  actions,
  getters
};

import { SET_BLOOD_RESOURCES } from '@/shared/types/mutations';
import { API_ALL_BLOOD_RESOURCES_GET } from '@/shared/types/endpoints';
import { FETCH_BLOOD_RESOURCES } from '@/shared/types/actions';
import { GET_BLOOD_RESOURCES } from '@/shared/types/getters';

const state = {
  bloodResources: []
};

const mutations = {
  [SET_BLOOD_RESOURCES] (state, bloodResources) {
    state.bloodResources = bloodResources;
  }
};

const actions = {
  async [FETCH_BLOOD_RESOURCES] ({ commit }) {
    const response = await fetch(API_ALL_BLOOD_RESOURCES_GET, {
      headers: {
        'Accept': 'application/json'
      }
    });

    const bloodResources = await response.json();
    commit(SET_BLOOD_RESOURCES, bloodResources);
  }
};

const getters = {
  [GET_BLOOD_RESOURCES]: state => state.bloodResources
};

export {
  state,
  mutations,
  actions,
  getters
};

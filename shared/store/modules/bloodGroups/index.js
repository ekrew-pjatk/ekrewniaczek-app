import { API_BLOOD_GROUPS_GET } from '@/shared/types/endpoints';
import { GET_BLOOD_GROUPS } from '@/shared/types/getters';
import { SET_BLOOD_GROUPS } from '@/shared/types/mutations';
import { FETCH_BLOOD_GROUPS } from '@/shared/types/actions';

const state = {
  bloodGroups: []
};

const mutations = {
  [SET_BLOOD_GROUPS] (state, bloodGroups) {
    state.bloodGroups = bloodGroups;
  }
};

const actions = {
  async [FETCH_BLOOD_GROUPS] ({ commit }) {
    const response = await fetch(API_BLOOD_GROUPS_GET, {
      headers: {
        'Accept': 'application/json'
      }
    });
    const bloodTypes = await response.json();

    commit(SET_BLOOD_GROUPS, bloodTypes);
  }
};

const getters = {
  [GET_BLOOD_GROUPS]: state => state.bloodGroups
};

export {
  state,
  mutations,
  actions,
  getters
};

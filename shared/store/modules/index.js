import * as centersModule from './centers';
import * as bloodGroupsModule from './bloodGroups';
import * as drawerModule from './drawer';
import * as bloodResourcesModule from './bloodResources';

export {
  centersModule,
  bloodGroupsModule,
  drawerModule,
  bloodResourcesModule
};

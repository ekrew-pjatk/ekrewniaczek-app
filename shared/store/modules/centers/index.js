import { GET_CENTERS, GET_CENTERS_IDS } from '@/shared/types/getters';
import { SET_CENTER, SET_CENTERS } from '@/shared/types/mutations';
import { FETCH_CENTER_BY_ID, FETCH_CENTERS, FETCH_CENTER_BLOOD_RESOURCES } from '@/shared/types/actions';
import { API_CENTERS_GET, API_BLOOD_RESOURCES_GET } from '@/shared/types/endpoints';

import { unwrap } from '@/shared/utils/api';

const state = {
  centers: [],
  centersMap: {}
};

const mutations = {
  [SET_CENTER] (state, center) {
    const contains = state.centers.some(c => center.centerId === c.centerId);
    if (!contains) {
      state.centers.concat(center.centerId);
      state.centersMap[center.centerId] = center;
    }
  },
  [SET_CENTERS] (state, centers) {
    state.centers = centers;
    centers.forEach(center => {
      state.centersMap[center.centerId] = center;
    });
  }
};

const actions = {
  async [FETCH_CENTERS] ({ commit }) {
    const response = await fetch(API_CENTERS_GET, {
      headers: { 'Accept': 'application/json' }
    });
    const centers = await response.json();

    commit(SET_CENTERS, centers);
  },

  async [FETCH_CENTER_BY_ID] ({ commit, getters }, id) {
    const response = await fetch(`${API_CENTERS_GET}/${id}`);
    const wrappedCenter = await response.json();
    const center = unwrap(wrappedCenter);

    const storeCenters = getters[GET_CENTERS_IDS];
    const contains = storeCenters.some(c => center.centerId === c.centerId);
    if (!contains) {
      commit(SET_CENTER, center);
    }

    return center;
  },

  async [FETCH_CENTER_BLOOD_RESOURCES] ({ commit }, id) {
    const url = API_BLOOD_RESOURCES_GET(id);
    const response = await fetch(url, {
      headers: {
        'Accept': 'application/json'
      }
    });

    const bloodResources = unwrap(await response.json());
    if (Array.isArray(bloodResources)) {
      const mappedBloodResources = bloodResources.map(br => ({
        blood: br.blood,
        volume: br.volume
      }));

      return mappedBloodResources;
    }
  }
};

const getters = {
  [GET_CENTERS_IDS]: state => state.centers,
  [GET_CENTERS]: state => state.centers
};

export {
  state,
  mutations,
  actions,
  getters
};

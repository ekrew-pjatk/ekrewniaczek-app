export const bloodResources = {
  CRITICAL: 500,
  LOW: 1000,
  MEDIUM: 1500,
  PLENTY: 3000
};

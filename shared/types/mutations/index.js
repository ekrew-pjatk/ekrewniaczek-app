const SET_ALERTS = 'SET_ALERTS';
const SET_PROXIMAL_ALERTS = 'SET_PROXIMAL_ALERTS';
const SET_ALERT_DETAIL = 'SET_ALERT_DETAIL';
const SET_CENTER = 'SET_CENTER';
const SET_CENTERS = 'SET_CENTERS';
const SET_BLOOD_GROUPS = 'SET_BLOOD_GROUPS';
const SET_BLOOD_RESOURCES = 'SET_BLOOD_RESOURCES';
const SET_PREFERENCES = 'SET_PREFERENCES';
const SET_DRAWER = 'SET_DRAWER';

export {
  SET_ALERTS,
  SET_PROXIMAL_ALERTS,
  SET_ALERT_DETAIL,
  SET_CENTERS,
  SET_CENTER,
  SET_BLOOD_GROUPS,
  SET_BLOOD_RESOURCES,
  SET_PREFERENCES,
  SET_DRAWER
};

import * as actions from './actions';
import * as getters from './getters';
import * as endpoints from './endpoints';
import * as mutations from './mutations';

export {
  actions,
  getters,
  endpoints,
  mutations
};

const BASE_URL = process.env.VUE_APP_BASE_URL;

const API_ALERTS_GET = `${BASE_URL}/search`;
const API_CENTERS_GET = `${BASE_URL}/centers`;
const API_BLOOD_GROUPS_GET = `${BASE_URL}/bloodgroups`;
const API_BLOOD_RESOURCES_GET = (id) => `${BASE_URL}/centers/${id}/bloodresources`;
const API_CENTERUSERS_CENTER_GET = (id) => `${BASE_URL}/centerusers/${id}/center`;
const API_ALL_BLOOD_RESOURCES_GET = `${BASE_URL}/bloodresources?limit=192`;

export {
  API_ALERTS_GET,
  API_CENTERS_GET,
  API_BLOOD_GROUPS_GET,
  API_BLOOD_RESOURCES_GET,
  API_CENTERUSERS_CENTER_GET,
  API_ALL_BLOOD_RESOURCES_GET
};

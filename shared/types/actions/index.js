const FETCH_ALERT = 'FETCH_ALERT';
const FETCH_ALERTS = 'FETCH_ALERTS';
const FETCH_ALERTS_PREFERRED = 'FETCH_ALERTS_PREFERRED';
const FETCH_ALERTS_PROXIMAL = 'FETCH_ALERTS_PROXIMAL';
const FETCH_CENTERS = 'FETCH_CENTERS';
const FETCH_CENTER_BY_ID = 'FETCH_CENTER_BY_ID';
const FETCH_CENTER_BLOOD_RESOURCES = 'FETCH_CENTER_BLOOD_RESOURCES';
const FETCH_BLOOD_GROUPS = 'FETCH_BLOOD_GROUPS';
const FETCH_BLOOD_RESOURCES = 'FETCH_BLOOD_RESOURCES';
const TOGGLED_DRAWER = 'TOGGLED_DRAWER';

export {
  FETCH_ALERT,
  FETCH_ALERTS,
  FETCH_ALERTS_PREFERRED,
  FETCH_ALERTS_PROXIMAL,
  FETCH_CENTERS,
  FETCH_CENTER_BY_ID,
  FETCH_CENTER_BLOOD_RESOURCES,
  FETCH_BLOOD_GROUPS,
  FETCH_BLOOD_RESOURCES,
  TOGGLED_DRAWER
};
